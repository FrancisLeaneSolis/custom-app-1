function requestAllUserOnLoad(client) {
    var settings = {
        url: '/api/v2/users.json?role=end-user',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            populateUserList(data.users);
            console.log(data.users);
        },
        function (response) {
            console.log(response);
        }
    );
}


function requestUserInfo(client, id) {
    var settings = {
        url: '/api/v2/users/' + id + '.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            populateUserDetails(data);
            console.log(data);
        },
        function (response) {
            console.log(response);
        }
    );
}

function requestTicketsByUser(client, user) {
    var settings = {
        url: '/api/v2/users/' + user + '/tickets/requested.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            populateTicketList(data.tickets)
            console.log(data);
        },
        function (response) {
            console.log(response);
        }
    );
}

function requestTicketsByID(client, id) {
    var settings = {
        url: '/api/v2/tickets/' + id + '.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log("SELECTED TICKET:");
            console.log(data.ticket);
            populateTicketDetails(data.ticket);
            requestTicketComments(client, id);
        },
        function (response) {
            console.log(response);
        }
    );
}

function requestTicketComments(client, id) {
    var settings = {
        url: '/api/v2/tickets/' + id + '/comments.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log('TICKETY COMMENTS:');
            console.log(data.comments);
            clearCommentsList();
            populateCommentsList(data.comments);
        },
        function (response) {
            console.log(response);
        }
    );
}


function requestFanClubOptions(client) {
    var settings = {
        url: '/api/v2/ticket_fields/360025323274/options.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log(data.custom_field_options);
            populateFanClubSelection(data.custom_field_options);
        },
        function (response) {
            console.log(response);
        }
    );
}


function requestCategoryMailOptions(client) {
    var settings = {
        url: '/api/v2/ticket_fields/360025315414/options.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log(data.custom_field_options);
            populateCategoryMailSelection(data.custom_field_options);
        },
        function (response) {
            console.log(response);
        }
    );
}

// SERVER POST FUNCTIONS

function createTickets(client) {
    var settings = {
        url: '/api/v2/tickets.json',
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            "ticket": {
                "subject": "TEST SUB",
                "comment": {
                    "body": "TEST BODY"
                }
            }
        })
    };

    client.request(settings).then(
        function (data) {
            console.log(data.ticket);
        },
        function (response) {
            showError(response);
        }
    );

}


function createUser(client) {
    var settings = {
        url: '/api/v2/tickets.json',
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            "ticket": {
                "subject": "TEST SUB",
                "comment": {
                    "body": "TEST BODY"
                }
            }
        })
    };

    client.request(settings).then(
        function (data) {
            console.log(data.ticket);
        },
        function (response) {
            showError(response);
        }
    );

}



// SERVER PUT FUNCTIONS
function updateTicketDetails(client, id, subject, comment, user_id , send_as_customer) {
    console.log("updateTicketDetails() run");
    var settings = {
        url: '/api/v2/tickets/' + id + '.json',
        type: 'PUT',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            "ticket": {
                "subject": subject,
                "comment": {
                    "body": comment,
                    "author_id": user_id
                },
                "custom_fields":[
                   {
                      "id":360025315454,
                       "value": send_as_customer
                    }
              ]
            }
        })
    };

    client.request(settings).then(
        function (data) {
            console.log("Updated the Ticket");
            console.log(data);
            requestTicketComments(client , id);
            updateTicketRow(id,subject);
        },
        function (response) {
            showError(response);
        }
    );

}