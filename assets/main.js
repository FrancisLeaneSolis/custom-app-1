$(function () {

    var client = ZAFClient.init();

    displayInitialForm();
    requestAllUserOnLoad(client);
    requestFanClubOptions(client);
    requestCategoryMailOptions(client);
    populateCommentsList();

});


// SERVER REQUEST FUNCTIONS

function requestAllUserOnLoad(client) {
    var settings = {
        //url: '/api/v2/users.json?role=end-user',
        url: '/api/v2/search.json?query=type:user role:end-user tags:user_francis',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            populateUserList(data.results);
            console.log(data.results);
        },
        function (response) {
            console.log(response);
        }
    );
}


function requestUserInfo(client, id) {
    var settings = {
        url: '/api/v2/users/' + id + '.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            populateUserDetails(data);
            console.log(data);
        },
        function (response) {
            console.log(response);
        }
    );
}

function requestTicketsByUser(client, user) {
    var settings = {
        url: '/api/v2/users/' + user + '/tickets/requested.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log("Request ticket by user success");
            console.log(data.tickets);
            populateTicketList(data.tickets);
            console.log("Request ticket by user success");
        },
        function (response) {
            console.log(response);
        }
    );
}

function requestTicketsByID(client, id) {
    var settings = {
        url: '/api/v2/tickets/' + id + '.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log("SELECTED TICKET:");
            console.log(data.ticket);
            populateTicketDetails(data.ticket);
        },
        function (response) {
            console.log(response);
        }
    );
}

function requestTicketComments(client, id) {
    var settings = {
        url: '/api/v2/tickets/' + id + '/comments.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log('TICKETY COMMENTS:');
            console.log(data.comments);
            populateCommentsList(data.comments);
            // console.log("REQUEST ALL TICKET START");
            //requestTicketsByUser(client, document.getElementById('ud_user_id_input').value);
            // console.log("REQUEST ALL TICKET END")
        },
        function (response) {
            console.log(response);
        }
    );
}


function requestFanClubOptions(client) {
    var settings = {
        url: '/api/v2/ticket_fields/360025323274/options.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log(data.custom_field_options);
            populateFanClubSelection(data.custom_field_options);
        },
        function (response) {
            console.log(response);
        }
    );
}


function requestCategoryMailOptions(client) {
    var settings = {
        url: '/api/v2/ticket_fields/360025315414/options.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            console.log(data.custom_field_options);
            populateCategoryMailSelection(data.custom_field_options);
        },
        function (response) {
            console.log(response);
        }
    );
}



// SERVER POST FUNCTIONS

function createTickets(client, subject, user_id, email, description, cat_mail, fan_club, user_name, mem_num, sender) {


    console.log('Create data by:User ID:' + user_id);
    console.log('Create data by:User Name:' + user_name);
    console.log('Create data by:User Email:' + email);
    console.log('Create data by:Mem Num:' + mem_num);
    console.log('Create data by:fan_club:' + fan_club);
    console.log('Create data by:Subject:' + subject);
    console.log('Create data by:Desc:' + description);
    console.log('Create data by:cat_mail:' + cat_mail);
    console.log('Create data by:sender:' + sender);

    if (fan_club == "none") {
        fan_club = null;
    }


    var settings = {
        url: '/api/v2/tickets.json',
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            "ticket": {
                "subject": subject,
                "requester_id": user_id,
                "comment": {
                    "body": description,
                    "author_id": user_id
                },
                "custom_fields": [{
                        "id": 360025323274,
                        "value": fan_club
                    },
                    {
                        "id": 360025315854,
                        "value": "user_francis"
                    },
                    {
                        "id": 360025323294,
                        "value": mem_num
                    },
                    {
                        "id": 360025323814,
                        "value": user_name
                    },
                    {
                        "id": 360025315414,
                        "value": cat_mail
                    },
                    {
                        "id": 360025315454,
                        "value": sender
                    },
                    {
                        "id": 360025304513,
                        "value": email
                    }
                ]
            }
        })
    };

    client.request(settings).then(
        function (data) {
            console.log("Created New Ticket");
            console.log(data);
            console.log("Created Ticket ID:" + data.ticket.id);
            requestTicketsByUser(client, user_id);
            requestTicketComments(client, data.ticket.id);
        },
        function (response) {
            console.log("ERROR CREATING TICKETS");
            console.log(response);
        }
    );

}


function createUser(client, name, email, fan_category, mem_num) {
    var settings = {
        url: '/api/v2/users.json',
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            "user": {
                "name": name,
                "email": email,
                "user_fields": {
                    "fan_category": fan_category,
                    "mem_num": mem_num,
                    "app_select": "user_francis"
                }
            }
        })
    };

    client.request(settings).then(
        function (data) {
            console.log('Created New User');
            console.log(data);
            requestAllUserOnLoad(client);
        },
        function (response) {
            console.log("ERROR CREATING USERS");
        }
    );

}



// SERVER PUT FUNCTIONS
function updateTicketDetails(client, id, subject, comment, user_id, send_as_customer) {
    console.log("updateTicketDetails() run");
    var settings = {
        url: '/api/v2/tickets/' + id + '.json',
        type: 'PUT',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            "ticket": {
                "subject": subject,
                "comment": {
                    "body": comment,
                    "author_id": user_id
                },
                "custom_fields": [{
                    "id": 360025315454,
                    "value": send_as_customer
                }]
            }
        })
    };

    client.request(settings).then(
        function (data) {
            console.log("Updated the Ticket");
            console.log(data);
            requestTicketComments(client, id);

        },
        function (response) {
            console.log("ERROR UPDATING TICKETS");
        }
    );;

}

// SET FUNCTIONS

function setDescriptionCommentLbl(isDescription, isComment) {
    if (isDescription == true) {
        document.getElementById('description_comment_lbl').innerHTML = '説明';
    } else if (isComment == true) {
        document.getElementById('description_comment_lbl').innerHTML = 'コメント';
    }
}

/*
function updateTicketRow(ticket_id, subject) {
    var table = document.getElementById("tickets_list");
    var target_cell = 2;
    var flag = false;
    for (var i = 0, row; row = table.rows[i]; i++) {
        //iterate through rows
        //rows would be accessed using the "row" variable assigned in the for loop
        for (var j = 0, col; col = row.cells[j]; j++) {
            //iterate through columns
            //columns would be accessed using the "col" variable assigned in the for loop
            if (col.innerHTML == ticket_id) {
                flag = true;
            }

            //SUBJECT COLUMN = 2
            if (j == target_cell && flag == true) {
                col.innerHTML = subject;
                console.log("Update Ticket Row:" + col.innerHTML);
                break;
            }
        }
    }
}
*/

function populateUserList(users_raw) {
    clearUserList();

    var filtered_user_info = [];
    for (var ctr = 0; ctr < users_raw.length; ctr++) {
        filtered_user_info.push([users_raw[ctr].id, users_raw[ctr].name]);
    }

    var table_data = filtered_user_info;
    var table = document.getElementById("user_list");
    for (var i = 0; i < table_data.length; i++) {
        // create a new row
        var newRow = table.insertRow(table.length);
        for (var j = 0; j < table_data[i].length; j++) {
            // create a new cell
            var cell = newRow.insertCell(j);

            // add value to the cell
            cell.innerHTML = table_data[i][j];
        }
    }
}

function populateTicketList(tickets_raw) {
    clearTicketList();

    var filtered_ticket_info = [];
    for (var ctr = 0; ctr < tickets_raw.length; ctr++) {
        filtered_ticket_info.push([tickets_raw[ctr].id, tickets_raw[ctr].ticket_form_id, tickets_raw[ctr].subject]);
    }

    var table_data = filtered_ticket_info;
    var table = document.getElementById("tickets_list");
    for (var i = 0; i < table_data.length; i++) {
        // create a new row
        var newRow = table.insertRow(table.length);
        for (var j = 0; j < table_data[i].length; j++) {
            // create a new cell
            var cell = newRow.insertCell(j);

            // add value to the cell
            cell.innerHTML = table_data[i][j];
        }
    }

    addTicketsRowHandlers();
}

function populateCommentsList(comments_raw) {
    clearCommentsList();

    for (index in comments_raw) {
        appendCommentsList(comments_raw[index].created_at, comments_raw[index].body);
    }


}

function appendCommentsList(title, content) {
    var ul = document.getElementById("comments_list");
    var li = document.createElement("li");
    li.setAttribute('class', 'list-group-item');
    li.innerHTML = '<b>' + title + '</b><p>' + content + '</p>';
    ul.prepend(li);
}



function populateFanClubSelection(fanclubs) {
    var select = document.getElementById("ud_fan_club_selection");
    for (index in fanclubs) {
        select.options[select.options.length] = new Option(fanclubs[index].name, fanclubs[index].value);
    }
}

function populateCategoryMailSelection(cat_mails) {
    var select = document.getElementById("td_cat_mail_selection");
    for (index in cat_mails) {
        select.options[select.options.length] = new Option(cat_mails[index].name, cat_mails[index].value);
    }
}

function populateUserDetails(data) {
    document.getElementById('ud_user_id_input').value = data.user.id;
    document.getElementById('ud_name_input').value = data.user.name;
    document.getElementById('ud_email_input').value = data.user.email;
    document.getElementById('ud_membership_no_input').value = data.user.user_fields.mem_num;
    document.getElementById('ud_fan_club_selection').value = data.user.user_fields.fan_category;
}

function populateTicketDetails(data) {
    var custom_fields = data.custom_fields;
    for (index in custom_fields) {
        if (custom_fields[index].id == "360025315414") {
            document.getElementById('td_cat_mail_selection').value = custom_fields[index].value;
        }
    }

    document.getElementById('td_title').value = data.subject;
}


function clearUserDetailsForm() {
    document.getElementById('ud_user_id_input').value = "";
    document.getElementById('ud_name_input').value = "";
    document.getElementById('ud_email_input').value = "";
    document.getElementById('ud_membership_no_input').value = "";
    document.getElementById('ud_fan_club_selection').value = "none";
}

function clearUserList() {
    var table = document.getElementById("user_list");
    //or use :  var table = document.all.tableid;
    for (var i = table.rows.length - 1; i > 1; i--) {
        table.deleteRow(i);
    }
}

function clearTicketList() {
    var table = document.getElementById("tickets_list");
    //or use :  var table = document.all.tableid;
    for (var i = table.rows.length - 1; i > 1; i--) {
        table.deleteRow(i);
    }
}

function clearTicketDetails() {
    document.getElementById('td_cat_mail_selection').value = "none";
    document.getElementById('td_title').value = "";
    document.getElementById('td_desc_comment_input').value = "";
}

function clearCommentsList() {
    console.log("DELETE COMMENTS");
    var comments_list = document.getElementById('comments_list');
    comments_list.innerHTML = "";
}


// UI POLICIES

function displayInitialForm() {
    document.getElementById("user_details_panel").style.visibility = "hidden";
    document.getElementById("tickets_panel").style.visibility = "hidden";
    document.getElementById("ticket_details_panel").style.visibility = "hidden";
    document.getElementById("history_panel").style.visibility = "hidden";
    setEditableUserDetails();
}

function displayCreateUserForms() {
    document.getElementById("user_details_panel").style.visibility = "visible";
    document.getElementById("user_id").style.visibility = "hidden";
    document.getElementById("tickets_panel").style.visibility = "hidden";
    document.getElementById("ticket_details_panel").style.visibility = "hidden";
    document.getElementById("history_panel").style.visibility = "hidden";
    document.getElementById("create_ticket_btn").classList.add("d-none");
    document.getElementById("create_user_btn").classList.remove("d-none");
    setEditableUserDetails();
}

function displayOpenUserForms() {
    document.getElementById("user_details_panel").style.visibility = "visible";
    document.getElementById("user_id").style.visibility = "visible";
    document.getElementById("tickets_panel").style.visibility = "visible";
    document.getElementById("ticket_details_panel").style.visibility = "hidden";
    document.getElementById("history_panel").style.visibility = "hidden";
    document.getElementById("create_ticket_btn").classList.remove("d-none");
    document.getElementById("create_user_btn").classList.add("d-none");

}

function displayCreateTicketFromUserForms() {
    document.getElementById("user_details_panel").style.visibility = "visible";
    document.getElementById("user_id").style.visibility = "visible";
    document.getElementById("tickets_panel").style.visibility = "visible";
    document.getElementById("ticket_details_panel").style.visibility = "visible";
    document.getElementById("history_panel").style.visibility = "hidden";
    document.getElementById("create_ticket_btn").classList.remove("d-none");
    document.getElementById("create_user_btn").classList.add("d-none");
    document.getElementById("td_send_btn").classList.remove("d-none");
    document.getElementById("td_update_btn").classList.add("d-none");
    setReadOnlyUserDetails();
    setEditableCategoryMail();
    setDescriptionCommentLbl(true, false);
}

function displayCreateTicketForms() {
    document.getElementById("user_details_panel").style.visibility = "visible";
    document.getElementById("user_id").style.visibility = "hidden";
    document.getElementById("tickets_panel").style.visibility = "hidden";
    document.getElementById("ticket_details_panel").style.visibility = "visible";
    document.getElementById("history_panel").style.visibility = "hidden";
    document.getElementById("create_ticket_btn").classList.add("d-none");
    document.getElementById("create_user_btn").classList.add("d-none");
    document.getElementById("td_send_btn").classList.remove("d-none");
    document.getElementById("td_update_btn").classList.add("d-none");
    setEditableUserDetails();
    setEditableCategoryMail();
    setDescriptionCommentLbl(true, false);
}

function displayOpenTicketForms() {
    document.getElementById("user_details_panel").style.visibility = "visible";
    document.getElementById("user_id").style.visibility = "visible";
    document.getElementById("tickets_panel").style.visibility = "visible";
    document.getElementById("ticket_details_panel").style.visibility = "visible";
    document.getElementById("history_panel").style.visibility = "visible";
    document.getElementById("create_ticket_btn").classList.remove("d-none");
    document.getElementById("create_user_btn").classList.add("d-none");
    document.getElementById("td_send_btn").classList.add("d-none");
    document.getElementById("td_update_btn").classList.remove("d-none");
    setReadOnlyUserDetails();
    setReadOnlyCategoryMail();
    setDescriptionCommentLbl(false, true);
}

function setReadOnlyUserDetails() {
    document.getElementById('ud_name_input').disabled = true;
    document.getElementById('ud_email_input').disabled = true;
    document.getElementById('ud_fan_club_selection').disabled = true;
    document.getElementById('ud_membership_no_input').disabled = true;
    document.getElementById('ud_user_id_input').disabled = true;
}

function setEditableUserDetails() {
    document.getElementById('ud_name_input').disabled = false;
    document.getElementById('ud_email_input').disabled = false;
    document.getElementById('ud_fan_club_selection').disabled = false;
    document.getElementById('ud_membership_no_input').disabled = false;
    document.getElementById('ud_user_id_input').disabled = false;
}

function setReadOnlyCategoryMail() {
    document.getElementById('td_cat_mail_selection').disabled = true;
}

function setEditableCategoryMail() {
    document.getElementById('td_cat_mail_selection').disabled = false;
}

function validateUserDetails() {
    var flag = true;
    if (document.getElementById('ud_name_input').value == "" ||
        document.getElementById('ud_email_input').value == "" ||
        document.getElementById('ud_fan_club_selection').value == "none" ||
        document.getElementById('ud_membership_no_input').value == "") {
        flag = false;
    }
    return flag;
}

function validateTicketDetails() {
    var flag = true;
    if (document.getElementById('td_desc_comment_input').value == "" ||
        document.getElementById('td_cat_mail_selection').value == "none" ||
        document.getElementById('td_title').value == "") {
        flag = false;
    }
    return flag;
}

function validateEmailFormat(email) {
    console.log("Validating email:"+ /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email));
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}
// ON CLICK FUNCTIONS

function onClickCreateBtn() {
    var create_sel = document.getElementById('create_selection');
    if (create_sel.value == "user") {
        displayCreateUserForms();
    } else {
        displayCreateTicketForms();
    }
    clearUserDetailsForm();
    clearTicketDetails();
}

function onClickSearchUserBtn() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("search_value");
    filter = input.value.toUpperCase();
    table = document.getElementById("user_list");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function onClickCreateUserBtn() {

    if (validateUserDetails() == true) {
        var name = document.getElementById('ud_name_input').value;
        var email = document.getElementById('ud_email_input').value;
        var mem_num = document.getElementById('ud_membership_no_input').value;
        var fan_category = document.getElementById('ud_fan_club_selection').value;
        var client = ZAFClient.init();
        if (validateEmailFormat(email)==true) {
            console.log("EMAIL IS VALID");
            console.log("---name:" + name + "---email:" + email + "---mem_num:" + mem_num + "---fan_cat:" + fan_category);
            createUser(client, name, email, fan_category, mem_num);
        }else{
            console.log("EMAIL IS inVALID");
            var email_msg = "Please correct your email ..."
            $(".alert_mgs_container").append('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
            '      <p>' + email_msg + '</p>' +
            '      <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '            <span aria-hidden="true">&times;</span>' +
            '      </button>' +
            '</div>');
        }

    } else {
        var msg = 'Creating user was unsuccessfull. Please fill up empty fields on User Details Form before submitting...'
        console.log(msg);
        $(".alert_mgs_container").append('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
            '      <p>' + msg + '</p>' +
            '      <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '            <span aria-hidden="true">&times;</span>' +
            '      </button>' +
            '</div>');
    }
}

function onClickCreateTicketBtn() {
    displayCreateTicketFromUserForms();
    clearTicketDetails();
}

function onClickSendBtn() {


    if (validateUserDetails() == true && validateTicketDetails() == true) {
        //USER FIELDS
        var user_id = document.getElementById('ud_user_id_input').value;
        var user_name = document.getElementById('ud_name_input').value;
        var email = document.getElementById('ud_email_input').value;
        var mem_num = document.getElementById('ud_membership_no_input').value;
        var fan_club = document.getElementById('ud_fan_club_selection').value;

        // TICKET FIELDS
        var subject = document.getElementById('td_title').value;
        var description = document.getElementById('td_desc_comment_input').value;
        var cat_mail = document.getElementById('td_cat_mail_selection').value;
        var sender = document.getElementById('td_sender_selection').value;
        var isSendCustomer = false;
        if (sender == "send_as_customer") {
            isSendCustomer = true;
        }

        if (validateEmailFormat(email)==true) {
            console.log("EMAIL IS VALID");
            var client = ZAFClient.init();
            createTickets(client, subject, user_id, email, description, cat_mail, fan_club, user_name, mem_num, isSendCustomer);
            clearCommentsList();
            displayOpenTicketForms();
        } else {
            console.log("EMAIL IS inVALID");
        }


    } else {
        var msg = 'Creating user and ticket was unsuccessfull. Please make sure to fill up all  fields on User Details and Ticket Details Form before submitting...'
        console.log(msg);
        $(".alert_mgs_container").append('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
            '      <p>' + msg + '</p>' +
            '      <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '            <span aria-hidden="true">&times;</span>' +
            '      </button>' +
            '</div>');
    }

}

function onClickUpdateBtn() {
    var id = document.getElementById('td_ticket_id').innerHTML;
    var subject = document.getElementById('td_title').value;
    var comment = document.getElementById('td_desc_comment_input').value;
    var user_id = document.getElementById('ud_user_id_input').value;
    var cat_mail_selection = document.getElementById('td_sender_selection').value;
    var isSendCustomer = false;
    if (cat_mail_selection == "send_as_customer") {
        isSendCustomer = true;
    }
    console.log("isSendCustomer:" + cat_mail_selection + "----->" + isSendCustomer);
    var client = ZAFClient.init();
    updateTicketDetails(client, id, subject, comment, user_id, isSendCustomer);

}


document.getElementById('user_list').onclick = function (event) {
    event = event || window.event; //for IE8 backward compatibility
    var target = event.target || event.srcElement; //for IE8 backward compatibility
    while (target && target.nodeName != 'TR') {
        target = target.parentElement;
    }
    var cells = target.cells; //cells collection
    //var cells = target.getElementsByTagName('td'); //alternative
    if (!cells.length || target.parentNode.nodeName == 'THEAD') { // if clicked row is within thead
        return;
    }

    displayOpenUserForms();

    var user_id = cells[0].innerText;
    console.log("ID:" + user_id);

    var client = ZAFClient.init();
    requestUserInfo(client, user_id);
    requestTicketsByUser(client, user_id);
    setReadOnlyUserDetails();

}

/*
document.getElementById('tickets_list').onclick = function (event) {
    event = event || window.event; //for IE8 backward compatibility
    var target = event.target || event.srcElement; //for IE8 backward compatibility
    while (target && target.nodeName != 'TR') {
        target = target.parentElement;
    }
    //var cells = target.cells; //cells collection
    var cells = target.getElementsByTagName('td'); //alternative
    if (!cells.length || target.parentNode.nodeName == 'THEAD') { // if clicked row is within thead
        return;
    }

    displayOpenTicketForms();
    clearTicketDetails();

    var ticket_id = cells[0].innerText;
    document.getElementById('td_ticket_id').innerHTML = ticket_id;
    console.log("Ticket ID:" + ticket_id);

    var client = ZAFClient.init();
    requestTicketsByID(client, ticket_id);
    // requestTicketComments(client, ticket_id);

}*/


function addTicketsRowHandlers() {
    var table = document.getElementById("tickets_list");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = function (row) {
            return function () {
                var cell = row.getElementsByTagName("td")[0];
                var id = cell.innerHTML;
                console.log("TICKET ID:" + id);
                displayOpenTicketForms();
                clearTicketDetails();
                document.getElementById('td_ticket_id').innerHTML = id;

                var client = ZAFClient.init();
                requestTicketsByID(client, id);
                requestTicketComments(client, id);
            };
        };
        currentRow.onclick = createClickHandler(currentRow);
    }
}

/*
function createTicketClickHandler(row){
    var cell = row.getElementsByTagName("td")[0];
    var id = cell.innerHTML;
    console.log("TICKET ID:" + id);
    displayOpenTicketForms();
    clearTicketDetails();
    document.getElementById('td_ticket_id').innerHTML = id;

    var client = ZAFClient.init();
    requestTicketsByID(client, id);
    // requestTicketComments(client, ticket_id);
}*/